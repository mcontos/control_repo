#! /usr/bin/env bash

if which puppet > /dev/null 2>&1; then
    echo 'Puppet Installed.'
else
    echo 'Installing Puppet.'
    wget -q https://apt.puppetlabs.com/puppet5-release-xenial.deb -O /tmp/puppetlabs.deb 
    dpkg -i /tmp/puppetlabs.deb > /dev/null
    apt-get update > /dev/null
    apt-get install puppetserver -y > /dev/null 
    service puppetserver start > /dev/null 2>&1 
fi

