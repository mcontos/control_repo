#! /usr/bin/env bash

if which puppet > /dev/null 2>&1; then
    echo 'Puppet Installed.'
else
    echo 'Installing Puppet.'
    curl -O https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
    sudo dpkg -i puppetlabs-release-pc1-xenial.deb
    sudo apt-get update
    sudo apt-get install puppetserver -y
fi

