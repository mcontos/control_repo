#! /usr/bin/env bash

if which puppet > /dev/null 2>&1; then
    echo 'Puppet Installed.'
else
    echo 'Installing Puppet.'
    wget https://apt.puppetlabs.com/puppet5-release-xenial.deb
    sudo dpkg -i puppet5-release-xenial.deb
    sudo apt-get update
    sudo apt-get install puppet-agent -y
    sudo service puppet start
fi

