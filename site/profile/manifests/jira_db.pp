class profile::jira_db {
  include '::mysql::server'

  mysql::db { 'jira_db':
    user     => 'jira_db_user',
    password => 'test1234',
    host     => 'localhost'
  }

}
